package com.butte.auto.client.timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

// @Component
public class HttpServiceJob {

    private static final Logger LOG = LoggerFactory.getLogger(HttpServiceJob.class.getName()) ;

    private static final String SERVER_NAME = "http://app-service:8082/serve";
    private static final String SERVER_IP = "http://10.103.252.94:8082/serve";

    /**
     * 启动类注解：@EnableScheduling
     * 每30秒执行一次
     */
    // @Scheduled(fixedDelay = 30000)
    public void systemDate () {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setReadTimeout(3000);
        factory.setConnectTimeout(6000);
        RestTemplate restTemplate = new RestTemplate(factory);

        try {
            Map<String, String> paramMap = new HashMap<>();
            String result = restTemplate.getForObject(SERVER_NAME, String.class, paramMap);
            LOG.info("service-name-resp::::" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Map<String, String> paramMap = new HashMap<>();
            String result = restTemplate.getForObject(SERVER_IP, String.class, paramMap);
            LOG.info("service-ip-resp::::" + result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

# butte-auto

## 1、案例简介

分布式服务的部署是一个复杂的流程，用手动的方式部署显然难度过高，基于Jenkins、Docker、K8S持续集成组件，实现自动化管理源码编译、打包、镜像构建、部署等系列操作；

![](https://images.gitee.com/uploads/images/2022/0116/201607_5bb9776e_5064118.png)

## 2、参考文章

- [自动化集成：Jenkins管理工具详解](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A01、Jenkins管理工具.md)
- [自动化集成：Pipeline流水语法详解](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A02、Pipeline流水线语法.md)
- [自动化集成：Docker容器入门简介](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A03、Docker容器简介.md)
- [自动化集成：Pipeline整合Docker容器](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A04、Docker集成流水线.md)
- [自动化集成：Kubernetes容器引擎详解](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A05、K8S容器引擎简介.md)
- [自动化集成：Pipeline整合Docker+K8S](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A06、K8S集成流水线.md)
- [K8S-核心应用原理分析](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A07、K8S应用原理分析.md)
- [K8S-容器和Pod组件](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A08、容器和Pod组件.md)
- [K8S-Deployment应用编排](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A09、Deployment应用.md)
- [K8S-Service服务发现](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A10、Service服务发现.md)
- [K8S-Config应用配置](https://gitee.com/cicadasmile/butte-java-note/blob/master/doc/frame/auto/A11、Config应用配置.md)

## 3、仓库整合

| 仓库 | 描述 |
|:---|:---|
| [butte-java](https://gitee.com/cicadasmile/butte-java-note) |Java编程文档整理，基础、架构，大数据 |
| [butte-frame](https://gitee.com/cicadasmile/butte-frame-parent) |微服务组件，中间件，常用功能二次封装 |
| [butte-flyer](https://gitee.com/cicadasmile/butte-flyer-parent) |butte-frame二次浅封装，实践案例 |
| [butte-auto](https://gitee.com/cicadasmile/butte-auto-parent) |Jenkins+Docker+K8S实现自动化持续集成 |
| [java-base](https://gitee.com/cicadasmile/java-base-parent) | Jvm、Java基础、Web编程，JDK源码分析 |
| [model-struct](https://gitee.com/cicadasmile/model-arithmetic-parent) | 设计模式、数据结构、算法 |
| [data-manage](https://gitee.com/cicadasmile/data-manage-parent) | 架构设计，实践，数据管理、工具 |
| [spring-mvc](https://gitee.com/cicadasmile/spring-mvc-parent) | Spring+Mvc框架基础总结 |
| [spring-boot](https://gitee.com/cicadasmile/spring-boot-base) | SpringBoot2基础，应用、配置等 |
| [middle-ware](https://gitee.com/cicadasmile/middle-ware-parent) | SpringBoot2进阶，整合常用中间件 |
| [spring-cloud](https://gitee.com/cicadasmile/spring-cloud-base) | Spring+Ali微服务基础组件用法|
| [cloud-shard](https://gitee.com/cicadasmile/cloud-shard-jdbc) | SpringCloud实现分库分表实时扩容 |
| [husky-cloud](https://gitee.com/cicadasmile/husky-spring-cloud) | SpringCloud综合入门案例 |
| [big-data](https://gitee.com/cicadasmile/big-data-parent) | Hadoop框架，大数据组件，数据服务 |
| [mysql-base](https://gitee.com/cicadasmile/mysql-data-base) | MySQL数据库基础、进阶总结 |
| [linux-system](https://gitee.com/cicadasmile/linux-system-base) | Linux系统基础，环境搭建、配置 |